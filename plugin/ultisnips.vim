" File: ultisnips.vim
" Author: JFH
" Description: Extra configuration for ultisnips. 
" Last Modified: November 03, 2020

"Trigger configuration. You need to change this to something other than <tab> if you use one of the following:
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"
let g:snips_author="jfh"
" Show list of snippets for this filetype.
let g:UltiSnipsListSnippets="<c-]>"

" Add any custom snippets here if you want.
