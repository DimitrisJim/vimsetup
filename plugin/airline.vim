"================= Airline settings ======================
" i.e the cool shiny underline on vim.

" Display all open buffers if there's only one tab open.
let g:airline#extensions#tabline#enabled = 1
" I really should find how to edit the statusline.
set t_Co=256
