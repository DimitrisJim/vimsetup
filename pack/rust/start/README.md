Plugins for Rust:
=================

Plugins for working with `rust` source code.

rust-vim:
---------

See [Github][rustvimgh] for more.

Vim plugin for Rust provided by the rust team.

coc.nvim:
---------

Indirectly through the extention `coc-rust-analyzer` that uses [`racer`][racergh]. 

syntastic:
----------

P.s do I actually need this?


[racergh]: https://github.com/racer-rust/racer
[rustvimgh]: https://github.com/rust-lang/rust.vim
