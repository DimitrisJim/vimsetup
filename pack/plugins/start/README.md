General Plugins:
================

ultisnips:
----------

See [Github][vimusnips] for more.

Snippet engine. Comes in handy for repetitive operations.

vim-snippets:
-------------

See [Github][vimsnips] for more.

Snippets for ultisnips. Has many snippets for different languages.

ale: 
----

See [Github][vimale] for more.

Using it mainly for linters/fixers. 

coc.nvim:
---------

See [Github][cocnvim] for more.

Implements Language Server Protocol. Has support for many languages and was quite 
easy to set-up.

vim-gitgutter:
--------------

See [Github][vimgitgut] for more.

Inline (i.e side column) git diffs.

vim-airline:
------------

See [Github][vimairline] for More.

Add cooler little status-line.

vim-obsession + vim-prosession:
-------------------------------

See [here][vimobs] and [here][vimpros] for more.

Handling sessions in vim.

rainbow_parentheses:
--------------------

See [Github][vimrbp] for more.

Alternating colours for embedded parentheses. Needs to be toggled on with `RainbowParenthesesToggle`. 

preview-markdown:
-----------------

See [Github][vimpreviewmark] for more.

Previews markdown inside terminal by using `mdr` (download binary and move to `/usr/local/bin` for example.
Does not automatically reload or do any other 'fancy' things. `PreviewMarkdown` is only command available; launches in a new :term and you can Control C your way out of it.

vim-gitignore:
--------------

See [Github][vimgitignore] for more.

Adds syntax highlighting for `.gitignore` files. Can also be used to autogen snippets but I haven't added necessary packages for that.

[vimusnips]: https://github.com/sirver/ultisnips
[vimsnips]: https://github.com/honza/vim-snippets
[vimairline]: https://github.com/vim-airline/vim-airline/
[vimgitgut]: https://github.com/airblade/vim-gitgutter
[cocnvim]: https://github.com/neoclide/coc.nvim
[vimpros]: https://github.com/dhruvasagar/vim-prosession
[vimobs]: https://github.com/tpope/vim-obsession
[vimrbp]: https://github.com/kien/rainbow_parentheses.vim
[vimpreviewmark]: https://github.com/skanehira/preview-markdown.vim
[vimgitignore]: https://github.com/gisphm/vim-gitignore
[vimale]: https://github.com/dense-analysis/ale
